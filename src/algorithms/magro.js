const R = require('ramda');
const data = require('./data/PCM3');

/** Converts numeric degrees to radians */
if (typeof (Number.prototype.toRad) === "undefined") {
    Number.prototype.toRad = function () {
        return this * Math.PI / 180;
    }
}

const heversine = R.curry((p1, p2) => {
    const Radius = 6371e3;
    const P1 = p1.lat.toRad();
    const P2 = p2.lat.toRad();

    const dP = (p2.lat - p1.lat).toRad();
    const dL = (p2.lng - p1.lng).toRad();

    const a = Math.sin(dP / 2) * Math.sin(dP / 2) +
        Math.cos(P1) * Math.cos(P2) *
        Math.sin(dL / 2) * Math.sin(dL / 2);

    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const d = Radius * c;

    return d;
});

/**
 * makeDistanceMatrix :: [a] -> [[a]]
 */
function makeDistanceMatrix(arrayofPoints) {
    const distanceMatrix = Array(arrayofPoints.length).fill(Array(arrayofPoints.length).fill(0));

    for (let i = 0; i < distanceMatrix.length; i++) {
        for (let j = 0; j < distanceMatrix.length; j++) {
            if (distanceMatrix[i][j] === 0) {
                distanceMatrix[i][j] = heversine(arrayofPoints[i], arrayofPoints[j]);
                distanceMatrix[j][i] = distanceMatrix[i][j];
            }
        }
    }

    return distanceMatrix;
}

const arePointsEqual = R.curry((p1, p2) => p1.lng === p2.lng && p1.lat === p2.lat);

/**
 * pathFinder :: {lat: Number, lng: Number} -> {lat: Number, lng: Number} -> [{lat: Number, lng: Number}]
 */
const pathFinder = R.curry((ways, p1, p2) => {
    let currentLocation = R.clone(p1);
    const flattenedWays = [p1, p2, ...ways];
    const distanceMatrix = makeDistanceMatrix(flattenedWays);
    const path = [currentLocation];

    while (!arePointsEqual(currentLocation, p2)) {
        const currentLocationIndex = R.findIndex(arePointsEqual(currentLocation), flattenedWays);
        const p2LocationIndex = 1;

        const remainingDistance = distanceMatrix[currentLocationIndex][p2LocationIndex];

        let i = 1;
        const setOfNextPoints = [];
        const setOfNextPointsIndexes = [];

        while (setOfNextPoints.length === 0) {
            for (let j = 0; j < distanceMatrix.length; j++) {
                if (distanceMatrix[currentLocationIndex][j] < 25 * i &&
                    !R.contains(flattenedWays[j], path)) {
                    setOfNextPoints.push(flattenedWays[j]);
                    setOfNextPointsIndexes.push(j);
                }
            }
            i++;
        }

        // while (setOfNextPoints.length === 0) {
        //     for (let j = 0; j < distanceMatrix.length; j++) {
        //         if (j !== currentLocationIndex && 
        //             distanceMatrix[currentLocationIndex][j] < 10 * i &&
        //             !R.contains(flattenedWays[j], path) &&
        //             ((distanceMatrix[currentLocationIndex][j] + distanceMatrix[j][p2LocationIndex]) < remainingDistance* 1.5) ) {
        //             setOfNextPoints.push(flattenedWays[j]);
        //         }
        //     }
        //     i++;
        // }

        const arrayOfTotalDistances = [];

        for (let i = 0; i < setOfNextPoints.length; i++) {
            arrayOfTotalDistances.push(distanceMatrix[currentLocationIndex][setOfNextPointsIndexes[i]] +
                distanceMatrix[setOfNextPointsIndexes[i]][p2LocationIndex]);
        }

        let leastDistance = arrayOfTotalDistances[0];
        let leastDistanceIndex = 0;

        for (let i = 1; i < arrayOfTotalDistances.length; i++) {
            if (arrayOfTotalDistances[i] < leastDistance) {
                leastDistance = arrayOfTotalDistances[i];
                leastDistanceIndex = i;
            }
        }

        //const nextPointIndex = R.findIndex(arePointsEqual(R.last(setOfNextPoints)), flattenedWays);
        //const nextPoint = flattenedWays[nextPointIndex];
        const nextPoint = setOfNextPoints[leastDistanceIndex];
        const nextPointIndex = R.findIndex(arePointsEqual(nextPoint), flattenedWays);

        path.push(nextPoint);
        currentLocation = R.clone(nextPoint);

        if (distanceMatrix[nextPointIndex][currentLocationIndex] > 500) {
            //return google maps walking path
            //console.log('greater than 25');
            return undefined;
        }

    }

    return path;
});

const ways = data.ways.features.map(f => f.geometry.coordinates[0].map(c => ({ lat: c[1], lng: c[0] })));
const flattenedWays = R.flatten(ways);

const PathFinder = pathFinder(flattenedWays);

module.exports = {
    heversine,
    PathFinder
};