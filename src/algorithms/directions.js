import Polyline from '@mapbox/polyline';
const R = require('ramda');
const { Way } = require('../redux/reducers/Maps/way');

/**
 * makeWay :: String -> String -> [Coordinate] -> Way
 */
const makeWay = R.curry((color, name, coors) =>
    Way(
        {
            WayName: name,
            color: color
        },
        {
            WayName: name,
            inList: 'way',
            type: 'replace',
            list: coors
        }
    ));

/**
 * google :: String -> String -> Promise [[Coordinate]]
 */
const google = R.curry((from, to) => 
    fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${typeof from === 'object' ? from.lat + ',' + from.lng : from}&destination=${to}&travelmode=walking`)
        .then(rep => rep.json())
        .then(rep => rep.routes)
        .then(R.map(r => r.overview_polyline.points))
        .then(R.map(Polyline.decode))
        .then(R.map(route =>
            route.map((point, i) => ({
                CoordinateName: i.toString(),
                lat: point[0],
                lng: point[1]
            }))))
    .catch(err => console.log(err)));

module.exports = {
    google,
    makeWay
};