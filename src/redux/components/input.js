import InputField from '../../ui/input';
const { connect } = require('react-redux');

const FromField = connect(
    state => ({
        placeHolder: state.FromField.text.text,
        //location: state.FromField.text.text,
        prevLocations: state.prevLocations
    }),
    dispatch => ({
        onChangeText: text => dispatch({
            DirectionName: 'currentDirection',
            LocationName: 'FromLocation',
            type: 'CHANGE_LOCATION_TITLE',
            title: text
        })
    })
)(InputField);

const ToField = connect(
    state => ({
        placeHolder: state.ToField.text.text,
        //location: state.ToField.text.text,
        prevLocations: state.prevLocations
    }),
    dispatch => ({
        onChangeText: text => dispatch({
            DirectionName: 'currentDirection',
            LocationName: 'ToLocation',
            type: 'CHANGE_LOCATION_TITLE',
            title: text
        })
    })
)(InputField);

module.exports = {
    FromField,
    ToField
};