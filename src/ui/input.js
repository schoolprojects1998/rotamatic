import React from 'react';
const R = require('ramda');
const { View, TextInput, Button, StyleSheet } = require('react-native');

const styles = StyleSheet.create({
    inputContainer: {
        margin: 7,
        padding: 7,
        elevation: 1,
        backgroundColor: 'rgba(255,255,255,0.9)',
        width: 300,
        top: 40,
        borderRadius: 3,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: 'gray',
        shadowOffset: { height: 10, width: 10 }
    },
    input: {
        elevation: 1,
        width: '99%',
        marginTop: 'auto',
        marginBottom: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
});

const InputField = ({ placeHolder, location, prevLocations, onChangeText }) =>
    <View style={styles.inputContainer}>
        <TextInput
            underlineColorAndroid="rgba(255,255,255,0.9)"
            style={styles.input}
            placeholder={' ' + placeHolder}
            onChangeText={onChangeText} />
    </View>;

export default InputField;