import ui from '../../ui/Submit';
import Polyline from '@mapbox/polyline';
import { Location, Permissions } from 'expo';
const R = require('ramda');
const { connect } = require('react-redux');
const { Way } = require('../reducers/Maps/way');
const { PathFinder } = require('../../algorithms/magro');
const { google } = require('../../algorithms/directions');

const Submit = connect(
    state => ({
        text: state.Submit.text.text,
        From: (x => x ? x + ' malta' : x)(state.currentDirection.From.title),
        To: state.currentDirection.To.title + ' malta'
    }),
    null,
    ({ From, To, text }, { dispatch }) => ({
        text: text,
        onPress: _ => {
            dispatch({
                SubmitName: 'Submit',
                type: 'CHANGE_TEXT_TEXT',
                text: 'Loading...'
            });
            Permissions.askAsync(Permissions.LOCATION)
                .then(status => status.status !== 'granted' ? Promise.reject(status) : status)
                .then(_ => From ? Location.geocodeAsync(From).then(R.head) : Location.getCurrentPositionAsync({ enableHighAccuracy: true }).then(o => o.coords))
                .then(coord => Location.geocodeAsync(To).then(R.head).then(c => [coord, c]))
                .then(R.map(c => ({ lat: c.latitude, lng: c.longitude })))
                .then(([from, to]) => PathFinder(from, to))
                .then(points => points ? points.map((point, i) => ({
                    CoordinateName: i.toString(),
                    lat: point.lat,
                    lng: point.lng
                })) : From ? google(From, To).then(R.head) :
                        Location.getCurrentPositionAsync({ enableHighAccuracy: true }).then(o => o.coords).then(c => ({ lat: c.latitude, lng: c.longitude })).then(c => google(c, To).then(R.head)))
                .then(coors => (
                    dispatch({
                        LocationName: 'currentLocation',
                        CoordinateName: 'currentCoordinate',
                        type: 'CHANGE_LAT',
                        lat: coors[0].lat
                    }),
                    dispatch({
                        LocationName: 'currentLocation',
                        CoordinateName: 'currentCoordinate',
                        type: 'CHANGE_LNG',
                        lng: coors[0].lng
                    }),
                    coors
                ))
                .then(coors => Way(
                    {
                        WayName: 'way1',
                        color: 'red'
                    },
                    {
                        WayName: 'way1',
                        inList: 'way',
                        type: 'REPLACE',
                        list: coors
                    }
                ))
                .then(way => dispatch({
                    DirectionName: 'currentDirection',
                    inList: 'ways',
                    type: 'REPLACE',
                    list: [way]
                }))
                .then(_ => {
                    dispatch({
                        SubmitName: 'Submit',
                        type: 'CHANGE_TEXT_TEXT',
                        text: 'Go'
                    });
                })
                .catch(err => console.log(err))
        }
    })
)(ui);

module.exports = {
    Submit
};