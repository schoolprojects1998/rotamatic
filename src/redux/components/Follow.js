import ui from '../../ui/Follow'
const { connect } = require('react-redux');

const Follow = connect(
    state => ({
        text: state.Follow.text.text,
        updateUserLocation: state.updateUserLocation,
        userLocation: state.userLocation
    }),
    null,
    ({ updateUserLocation, text }, { dispatch }) => ({
        text: text,
        onPress: _ => {
            dispatch({
                FollowName: 'Follow',
                type: 'CHANGE_TEXT_TEXT',
                text: updateUserLocation ? 'Follow' : 'Unfollow'
            })
            dispatch({
                type: 'UPDATE_USER_LOCATION',
                value: !updateUserLocation
            });
        }
    })
)(ui);

module.exports = {
    Follow
};