const R = require('ramda');
const {Coordinate, change, changeName} = require('../redux-utils/index');

const Location = (state = {}, action) =>
    action.LocationName === state.LocationName ? {
        LocationName: changeName('LOCATION')(state.LocationName, action),
        Coordinate: Coordinate(state.Coordinate, action),
        title: change('LOCATION', 'TITLE')(state.title, action)
    } : state;

module.exports = {
    Location
};