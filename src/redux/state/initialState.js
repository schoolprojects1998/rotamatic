const defaultCoordinates = {
    lat: 35.873488,
    lng: 14.436327,
    latD: 0.02,
    lngD: 0.01
};

const initialState = {
    FromField: {
        InputfieldName: 'FromField',
        text: {
            text: 'My Location'
        }
    },
    ToField: {
        InputfieldName: 'ToField',
        text: {
            text: 'To'
        }
    },
    Map: {
        MapName: 'Map',
        defaultCenter: {
            CoordinateName: 'DefaultCoordinate',
            ...defaultCoordinates
        }
    },
    Submit: {
        SubmitName: 'Submit',
        text: {
            text: 'GO'
        }
    },
    Follow: {
        FollowName: 'Follow',
        text: {
            text: 'Follow'
        }
    },
    Alert: {
        AlertName: 'Alert',
        text: {
            text: 'Permission to access location denied'
        },
        hidden: true
    },
    userLocation: {
        LocationName: 'userLocation',
        Coordinate: {
            CoordinateName: 'userCoordinate',
            ...defaultCoordinates
        }
    },
    currentLocation: {
        LocationName: 'currentLocation',
        Coordinate: {
            CoordinateName: 'currentCoordinate',
            ...defaultCoordinates
        },
        title: 'Seattle'
    },
    searchedLocation: {
        LocationName: 'searchedLocation',
        Coordinate: {
            CoordinateName: 'searchedCoordinate',
            ...defaultCoordinates
        }
    },
    currentDirection: {
        DirectionName: 'currentDirection',
        From: {
            LocationName: 'FromLocation',
            Coordinate: {},
            title: ''
        },
        To: {
            LocationName: 'ToLocation',
            Coordinate: {},
            title: ''
        },
        ways: []
    },
    prevLocations: [],
    prevDirections: [],
    Compass: {
        CompassName: 'Compass',
        heading: 0
    },
    updateUserLocation: false
};

module.exports = {
    initialState
};