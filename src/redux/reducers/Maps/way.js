const { media, Coordinate, List, change } = require('../redux-utils/index');

const Way = (state = {}, action) =>
    state.WayName === action.WayName ? {
        ...media('WAY')(state, action),
        way: List(Coordinate, 'way')(state.way, action),
        color: change('WAY', 'COLOR')(state.color, action)
    } : state;

module.exports = {
    Way
};