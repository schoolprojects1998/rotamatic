import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Location, Permissions } from 'expo';
const { createStore } = require('redux');
const { Provider } = require('react-redux');

const { ToField, FromField } = require('./src/redux/components/input');
const { Map } = require('./src/redux/components/Map');
const { Submit } = require('./src/redux/components/Submit');
const { Follow } = require('./src/redux/components/Follow');

const { root } = require('./src/redux/reducers/root');
const { initialState } = require('./src/redux/state/initialState');

const styles = StyleSheet.create({
    overallViewContainer: {
        position: 'absolute',
        height: '100%',
        width: '100%',
    },
    allNonMapThings: {
        alignItems: 'center',
        height: '100%',
        width: '100%',
        justifyContent: 'space-between'
    },
    buttonsContainer: {
        flexDirection: 'row',

    },
    inputsContainer: {
        flex: 1,
    }
});

const store = createStore(root, initialState);

//store.subscribe(_ => console.log(store.getState()));

const Outer = _ =>
    <View style={styles.allNonMapThings}>
        <View style={styles.inputsContainer}>
            <FromField />
            <ToField />
        </View>
        <View style={styles.buttonsContainer}>
            <Submit />
            <Follow />
        </View>
    </View>;

const App = _ =>
    <Provider store={store}>
        <View style={styles.overallViewContainer}>
            <Map />
            <Outer />
        </View>
    </Provider>;

// Location.watchPositionAsync({ enableHighAccuracy: true, timeInterval: 1, distanceInterval: 1 }, function (coord) {
//     console.log(coord.coords)
//     store.dispatch({
//         LocationName: 'userLocation',
//         CoordinateName: 'userCoordinate',
//         type: 'CHANGE_LNG',
//         lng: coord.coords.longitude
//     });
//     store.dispatch({
//         LocationName: 'userLocation',
//         CoordinateName: 'userCoordinate',
//         type: 'CHANGE_LAT',
//         lat: coord.coords.latitude
//     });
//     store.dispatch({
//         CompassName: 'Compass',
//         type: 'CHANGE_HEADING',
//         heading: coord.coords.heading
//     });
// });

setInterval(function () {
    store.getState().updateUserLocation &&
        Location.getCurrentPositionAsync({ enableHighAccuracy: true })
            .then(o => o.coords)
            .then(c => (
                store.dispatch({
                    LocationName: 'userLocation',
                    CoordinateName: 'userCoordinate',
                    type: 'CHANGE_LNG',
                    lng: c.longitude
                }),
                store.dispatch({
                    LocationName: 'userLocation',
                    CoordinateName: 'userCoordinate',
                    type: 'CHANGE_LAT',
                    lat: c.latitude
                })
                // store.dispatch({
                //     LocationName: 'userLocation',
                //     CoordinateName: 'userCoordinate',
                //     type: 'CHANGE_LNGD',
                //     lngD: c.latitude
                // }),
                // store.dispatch({
                //     LocationName: 'userLocation',
                //     CoordinateName: 'userCoordinate',
                //     type: 'CHANGE_LATD',
                //     latD: c.lat
                // })
            ));
}, 100)

Permissions.askAsync(Permissions.LOCATION)
    .then(status => status.status == 'granted' &&
        Location.getCurrentPositionAsync({ enableHighAccuracy: true })
            .then(o => o.coords)
            .then(c => ({ lat: c.latitude, lng: c.longitude }))
            .then(c => (
                store.dispatch({
                    LocationName: 'currentLocation',
                    CoordinateName: 'currentCoordinate',
                    type: 'CHANGE_LNG',
                    lng: c.lng
                }),
                store.dispatch({
                    LocationName: 'currentLocation',
                    CoordinateName: 'currentCoordinate',
                    type: 'CHANGE_LAT',
                    lat: c.lat
                })
            ))
    );

export default App;