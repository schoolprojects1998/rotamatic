const {changeName, change} = require('../redux-utils/index');

const Compass = (state = {}, action) => 
    state.CompassName === action.CompassName ? {
        CompassName: changeName('COMPASS')(state.CompassName, action),
        heading: change('COMPASS', 'HEADING')(state.heading, action)
    } : state;

module.exports = {
    Compass
};