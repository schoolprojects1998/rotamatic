const { GoogleMap, List } = require('../redux-utils/index');
const { Location } = require('./location')
const { Way } = require('./way');

const Map = (state = {}, action) =>
    state.MapName === action.MapName ? {
        ...GoogleMap(state, action),
    } : state;

module.exports = {
    Map
};