import React from 'react';
const { View, StyleSheet, TouchableOpacity, Button, Text } = require('react-native');

const styles = StyleSheet.create({
    button: {
        margin: 10,
        elevation: 1,
        bottom: 25,
        backgroundColor: 'gray',
        borderRadius: 10,
        width: '40%',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        shadowOpacity: 0.75,
        shadowRadius: 1,
        shadowColor: 'gray',
        shadowOffset: { height: 0, width: 0 }
    },
    buttonText: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
    },
});

const Follow = ({onPress, text}) =>
    <View style={styles.button}>
        <TouchableOpacity onPress={onPress}>
            <Text style={styles.buttonText}>
                {text}
            </Text>
        </TouchableOpacity>
    </View>;

export default Follow;