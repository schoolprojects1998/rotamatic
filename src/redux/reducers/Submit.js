const { media } = require('../reducers/redux-utils/index');

const Submit = media('SUBMIT');

module.exports = {
    Submit
};