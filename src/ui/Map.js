import React from 'react';
import MapView, { Polyline, Circle, Marker } from 'react-native-maps';
const R = require('ramda');
const { StyleSheet, View, Text } = require('react-native');

const SIZE = 20;
const HALO_RADIUS = 6;
const ARROW_SIZE = 7;
const ARROW_DISTANCE = 6;
const HALO_SIZE = SIZE + HALO_RADIUS;
const HEADING_BOX_SIZE = HALO_SIZE + ARROW_SIZE + ARROW_DISTANCE;

const colorOfmyLocationMapMarker = 'blue';

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    markerContainer: {
        width: HEADING_BOX_SIZE,
        height: HEADING_BOX_SIZE,
    },
    heading: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: HEADING_BOX_SIZE,
        height: HEADING_BOX_SIZE,
        alignItems: 'center',
    },
    headingPointer: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderRightWidth: ARROW_SIZE * 0.75,
        borderBottomWidth: ARROW_SIZE,
        borderLeftWidth: ARROW_SIZE * 0.75,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: colorOfmyLocationMapMarker,
        borderLeftColor: 'transparent',
    },
    markerHalo: {
        position: 'absolute',
        backgroundColor: 'grey',
        top: 0,
        left: 0,
        width: HALO_SIZE,
        height: HALO_SIZE,
        borderRadius: Math.ceil(HALO_SIZE / 2),
        margin: (HEADING_BOX_SIZE - HALO_SIZE) / 2,
        shadowColor: 'black',
        shadowOpacity: 0.25,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1,
        },
    },
    marker: {
        justifyContent: 'center',
        backgroundColor: colorOfmyLocationMapMarker,
        width: SIZE,
        height: SIZE,
        borderRadius: Math.ceil(SIZE / 2),
        margin: (HEADING_BOX_SIZE - SIZE) / 2,
    },
});

const Map = ({ updateUserLocation, changeRegion, location, initialLocation, ways, userLocation, compass }) =>
    <MapView style={styles.container}
        initialRegion={{
            latitude: initialLocation.lat,
            longitude: initialLocation.lng,
            latitudeDelta: initialLocation.latD,
            longitudeDelta: initialLocation.lngD
        }}
        region={{
            latitude: (updateUserLocation ? userLocation : location).lat,
            longitude: (updateUserLocation ? userLocation : location).lng,
            latitudeDelta: location.latD,
            longitudeDelta: location.lngD
        }}
        onRegionChangeComplete={region => changeRegion(region)}
    >
        {ways.map(way =>
            <Polyline key={way.WayName}
                coordinates={way.way.map(c => ({
                    latitude: c.lat,
                    longitude: c.lng
                }))}
                strokeWidth={4}
                strokeColor={way.color} />
        )}
        <Marker
            anchor={{ x: 0.5, y: 0.5 }}
            style={styles.mapMarker}
            coordinate={{
                latitude: userLocation.lat,
                longitude: userLocation.lng
            }}>
            <View style={styles.markerContainer}>
                <View style={styles.markerHalo}>
                    {/* <View style={[styles.heading, { transform: [{ rotate: compass.rotate }] }]}>
                        <View style={styles.headingPointer} />
                    </View> */}
                </View>
            </View>
        </Marker>

        {/* <Circle center={{
            latitude: userLocation.lat,
            longitude: userLocation.lng
        }} radius={10} /> */}

    </MapView>;

export default Map;