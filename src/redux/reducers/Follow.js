const { media } = require('../reducers/redux-utils/index');

const Follow = media('FOLLOW');

module.exports = {
    Follow
};