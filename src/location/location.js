import { Alert } from 'react-native';
import { Constants, Location, Permissions } from 'expo';

const askPermission = _ =>
    Permissions.askAsync(Permissions.LOCATION)
        .then(({ status }) => status !== 'granted' ?
            new Promise(res =>
                Alert.alert(
                    'Permission to access location was denied',
                    '',
                    [
                        { text: 'OK', onPress: _ => askPermission().then(res) }
                    ]
                )) : status);

const updateCoordinate = (type, v) => ({
    LocationName: 'currentLocation',
    CoordinateName: 'currentCoordinate',
    type: type,
    lat: v
})

const updateCurrentLocation = store =>
    askPermission()
        .then(_ => Location.getCurrentPositionAsync({}))
        .then(({ coords }) =>
            (
                store.dispatch(updateCoordinate('CHANGE_LAT', coords.latitude)),
                store.dispatch(updateCoordinate('CHANGE_LNG', coords.longitude))
            ));

/**
 * getPoint :: String -> Promise {lat: Number, lng: Number}
 */
const getPoint = address =>
    Location.geocodeAsync(address)
    .then(coords => ({lat: coords[0].latitude, lng: coords[0].longitude}));       

module.exports = {
    updateCurrentLocation,
    getPoint
};