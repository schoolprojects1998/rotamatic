const { List } = require('./redux-utils/index');
const { Map } = require('./Maps/Map');
const { Location } = require('./Maps/location');
const { InputField } = require('./InputField');
const { Submit } = require('./Submit');
const { Follow } = require('./Follow');
const { Direction } = require('./Maps/Direction');
const { Alert } = require('./Alert');
const { Compass } = require('./Maps/Compass');

const root = (state = {}, action) => ({
    FromField: InputField(state.FromField, action),
    ToField: InputField(state.ToField, action),
    Map: Map(state.Map, action),
    Submit: Submit(state.Submit, action),
    Alert: Alert(state.Alert, action),
    userLocation: Location(state.userLocation, action),
    currentLocation: Location(state.currentLocation, action),
    searchedLocation: Location(state.searchedLocation, action),
    currentDirection: Direction(state.currentDirection, action),
    prevLocations: List(Location, 'prevLocations')(state.prevLocations, action),
    prevDirections: List(Direction, 'prevDirections')(state.prevDirections, action),
    Compass: Compass(state.Compass, action),
    updateUserLocation: ((state = false, action) =>
        action.type === `UPDATE_USER_LOCATION` ? action.value : state)(state.updateUserLocation, action),
    Follow: Follow(state.Follow, action)
});

module.exports = {
    root
};