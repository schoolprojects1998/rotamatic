const {media, change} = require('../reducers/redux-utils/index');

const Alert = (state = {}, action) =>
    state.AlertName === action.AlertName ? ({
        hidden: change('ALERT', 'hidden', true)(state.hidden, action),
        ...media('ALERT')(state, action)
    }) : state;

module.exports = {
    Alert
};