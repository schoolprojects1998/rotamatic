const { Location } = require('./location');
const { Way } = require('./way');
const { changeName, media, List } = require('../redux-utils/index');

const Direction = (state = {}, action) =>
    state.DirectionName === action.DirectionName ? {
        ...media('DIRECTION')(state, action),
        From: Location(state.From, action),
        To: Location(state.To, action),
        ways: List(Way, 'ways')(state.ways, action)
    } : state;

module.exports = {
    Direction
};