import ui from '../../ui/Map';
const { connect } = require('react-redux');

const Map = connect(
    state => ({
        location: state.currentLocation.Coordinate,
        initialLocation: state.currentLocation.Coordinate,
        ways: state.currentDirection.ways,
        userLocation: state.userLocation.Coordinate,
        compass: {
            rotate: `${state.Compass.heading}deg`
        },
        updateUserLocation: state.updateUserLocation
    }),
    dispatch => ({
        changeRegion: region => (
            dispatch({
                LocationName: 'currentLocation',
                CoordinateName: 'currentCoordinate',
                type: 'CHANGE_LAT',
                lat: region.latitude
            }),
            dispatch({
                LocationName: 'currentLocation',
                CoordinateName: 'currentCoordinate',
                type: 'CHANGE_LNG',
                lng: region.longitude
            }),
            dispatch({
                LocationName: 'currentLocation',
                CoordinateName: 'currentCoordinate',
                type: 'CHANGE_LATD',
                latD: region.latitudeDelta
            }),
            dispatch({
                LocationName: 'currentLocation',
                CoordinateName: 'currentCoordinate',
                type: 'CHANGE_LNGD',
                lngD: region.longitudeDelta
            })
        )
    })
)(ui);

module.exports = {
    Map
};