const {media} = require('../reducers/redux-utils/index');

const InputField = media('INPUTFIELD');

module.exports = {
    InputField
};